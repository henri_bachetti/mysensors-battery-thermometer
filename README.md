# MYSENSORS BATTERY THERMOMETER

The purpose of this page is to explain step by step the realization of a low power thermometer based on ARDUINO PRO MINI, connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

The board uses the following components :

 * an ARDUINO PRO MINI 3.3V 8MHz
 * a NRF24L01
 * a DS18B20 temperature sensor
 * a LM2936-3.3 regulator
 * some passive components
 * the board is powered by a 200mAH LITHIUM-ION battery.

In the version V1.0, the NRF24L01 is powered thru two diodes 1N4148 to step down the battery voltage.

In fact a LITHUIM-ION battery has a full charge voltage of 4.2V.

The 1N4148, when the current is very low (1µA), has a forward voltage of 270mV, so 540mV with two diodes. And 4.2V - 0.54V = 3.66V.

The datasheet of the NRF24L01 specifies a maximum power voltage range of 1.9V - 3.6V.

The regulator has been added in the version V1.1.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### VERSIONS

There is two versions :

 * V1.0 : a version without 3.3V regulator. It consumes 6µA.
 * V1.1 : a version with 3.3V regulator, more secure for the NRF24L01. It consumes 25µA.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/01/un-thermometre-mysensors-sur-batterie.html

